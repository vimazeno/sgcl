<?php

namespace Cli\Sgcl\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

use Cli\Output\Json;
use Cli\Sgcl\Services\SgclConfig;

class ConfigCommand extends Command {

  private $config;

  public function __construct(SgclConfig $sgclConfig)
  {
    parent::__construct();
    $this->config = $sgclConfig;
  }

  protected function configure()
  {
    $this->setName("config")
    ->setDescription("edit sgcl config file")
    ->setDefinition(array(
      new InputOption('edit', null, InputOption::VALUE_NONE, 'edit config file')
    ))
    ->setHelp("edit wsec config file");
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {

    if($input->getOption('edit'))
    {
      $process = new Process("vi ".$this->config->getConfigFile());
      try {
          $process->setTty(true);
          $process->mustRun(function ($type, $buffer) {
              echo $buffer;
          });
      } catch (ProcessFailedException $e) {
          echo $e->getMessage();
      }
    }
    else
    {
      $output->writeln(Json::indent(json_encode($this->config->getConfigFile())));
    }

  }

}
