<?php

namespace Cli\Sgcl\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;

use Cli\Sgcl\Services\HelloManager;
use Cli\Sgcl\Services\SgclConfig;

class HelloCommand extends Command {

  private $config;
  private $managers = Array();

  public function __construct(SgclConfig $sgclConfig)
  {
    parent::__construct();
    $this->config = $sgclConfig;
  }

  public function addManager($managerName, $manager)
  {
    $this->managers[$managerName] = $manager;
  }

  protected function configure()
  {
    $this->setName("hello")
    ->setDescription("display hello to user readed in conf")
    ->setHelp("display hello to user readed in conf");
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $output->writeln("<info>".$this->managers['hello']->hello($this->config->getUser())."</info>");
  }

}
