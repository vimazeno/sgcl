<?php

namespace Cli\Sgcl\Services;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Yaml\Yaml;


class SgclConfig
{
  private $configFile;
  private $configFilePath;

  public function __construct()
  {
    $this->setConfigFile();
    $this->loadConfigFile();
  }

  public function setConfigFile(){
    // locate config file path . ou ~
    if(file_exists('./config.yml'))
    {
      $this->configFile = realpath(__DIR__ . "/../../../config.yml");
      if(\Phar::running())
        dirname(\Phar::running(false)). "/config.yml";
    }
    elseif(file_exists($_SERVER['HOME'].'/.sgcl/config.yml'))
    {
      $this->configFile = realpath($_SERVER['HOME'].'/.sgcl/config.yml');
    }
    else {
      die("set a config file\n\n");
    }
  }

  public function loadConfigFile(){
    $config = Yaml::parse(file_get_contents($this->getConfigFile()));
    $this->user = $config['user'];
  }

  public function getConfigFile(){
    return $this->configFile;
  }

  public function getUser(){
    return $this->user;
  }

}
