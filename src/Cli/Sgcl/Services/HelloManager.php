<?php

namespace Cli\Sgcl\Services;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

use Cli\Sgcl\Services\SgclConfig;

class HelloManager
{

  public function hello($user)
  {
    return "hello ".$user;
  }

}
