## Sgcl

phar.readonly = Off dans php.ini

A command line tool to use ispconfig API

## development

### build phar

download box

```shell
curl -LSs https://box-project.github.io/box2/installer.php | php
```

```shell
box build -vv
```

```shell
sudo mv sgcl.phar /usr/local/bin/sgcl
```

N.B. only commited code will be packaged

## config.yml

```bash
username: mazenovi
```

## See Also

* [Distributing a PHP CLI app with ease ](https://moquet.net/blog/distributing-php-cli/)
* [Secure PHAR Automation](https://mwop.net/blog/2015-12-14-secure-phar-automation.html)
* [External config files with PHAR archives](https://gist.github.com/GaryRogers/735159ca0dd22621c638)
